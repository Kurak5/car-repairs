﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CarRepairs.DAL.Models
{
    public class Vehicle
    {
        public int Id { get; set; }

        [MaxLength(100)]
        [Required]
        public string Producent { get; set; }
        [MaxLength(100)]
        [Required]
        public string ModelName { get; set; }

        public int? ProductionYear { get; set; }

        public int Mileage { get; set; }

        public int? EngineCapacity { get; set; }

        public int? EnginePower { get; set; }

        [Required]
        public DateTime PurchaseDate { get; set; }

        [Required]
        public decimal PurchasePrice { get; set; }

        public string UserId { get; set; }

        public virtual ApplicationUser User { get; set; }

        public virtual ICollection<Repair> Repairs { get; set; }

        public virtual Image Image { get; set; }
    }
}