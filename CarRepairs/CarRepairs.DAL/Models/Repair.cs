﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarRepairs.DAL.Models
{
    public class Repair
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(200)]
        public string Place { get; set; }

        public int? VehicleMileage { get; set; }

        [Required]
        public int Labor { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [MaxLength(500)]
        public string Adnotation { get; set; }

        public int VehicleId { get; set; }

        [Required]
        public virtual Vehicle Vehicle { get; set; }

        public virtual ICollection<RepairPart> RepairParts { get; set; }
    }
}
