﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarRepairs.DAL.Models
{
    public class RepairPart
    {
        public int Id { get; set; }

        [MaxLength(300)]
        public string Name { get; set; }

        [Required]
        public decimal Cost { get; set; }

        [MaxLength(500)]
        public string Adnotation { get; set; }

        public int RepairId { get; set; }

        [Required]
        public virtual Repair Repair { get; set; }
    }
}