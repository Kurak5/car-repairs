﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarRepairs.DAL.Models
{
    public class Image
    {
        [ForeignKey("Vehicle")]
        public int Id { get; set; }

        [Required]
        [MaxLength(300)]
        public string Title { get; set; }

        [Required]
        public string Path { get; set; }

        public virtual Vehicle Vehicle { get; set; }
    }
}
