namespace CarRepairs.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateVehicle : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Vehicles", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Vehicles", new[] { "User_Id" });
            RenameColumn(table: "dbo.Vehicles", name: "User_Id", newName: "ApplicationUser_Id");
            AddColumn("dbo.Vehicles", "ApplicationUserId", c => c.Int(nullable: false));
            AlterColumn("dbo.Vehicles", "ApplicationUser_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Vehicles", "ApplicationUser_Id");
            AddForeignKey("dbo.Vehicles", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
            DropColumn("dbo.Vehicles", "UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Vehicles", "UserId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Vehicles", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Vehicles", new[] { "ApplicationUser_Id" });
            AlterColumn("dbo.Vehicles", "ApplicationUser_Id", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.Vehicles", "ApplicationUserId");
            RenameColumn(table: "dbo.Vehicles", name: "ApplicationUser_Id", newName: "User_Id");
            CreateIndex("dbo.Vehicles", "User_Id");
            AddForeignKey("dbo.Vehicles", "User_Id", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
    }
}
