namespace CarRepairs.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateImageTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Title = c.String(nullable: false, maxLength: 300),
                        Path = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vehicles", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Images", "Id", "dbo.Vehicles");
            DropIndex("dbo.Images", new[] { "Id" });
            DropTable("dbo.Images");
        }
    }
}
