namespace CarRepairs.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateImage : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Title = c.String(nullable: false, maxLength: 300),
                        Path = c.String(nullable: false),
                        VehicleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vehicles", t => t.Id)
                .Index(t => t.Id);
            
            AddColumn("dbo.Vehicles", "ImageId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Images", "Id", "dbo.Vehicles");
            DropIndex("dbo.Images", new[] { "Id" });
            DropColumn("dbo.Vehicles", "ImageId");
            DropTable("dbo.Images");
        }
    }
}
