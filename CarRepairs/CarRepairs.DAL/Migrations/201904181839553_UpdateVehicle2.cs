namespace CarRepairs.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateVehicle2 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Vehicles", name: "ApplicationUser_Id", newName: "UserId");
            RenameIndex(table: "dbo.Vehicles", name: "IX_ApplicationUser_Id", newName: "IX_UserId");
            DropColumn("dbo.Vehicles", "ApplicationUserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Vehicles", "ApplicationUserId", c => c.Int(nullable: false));
            RenameIndex(table: "dbo.Vehicles", name: "IX_UserId", newName: "IX_ApplicationUser_Id");
            RenameColumn(table: "dbo.Vehicles", name: "UserId", newName: "ApplicationUser_Id");
        }
    }
}
