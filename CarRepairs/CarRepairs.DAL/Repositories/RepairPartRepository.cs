﻿using CarRepairs.DAL;
using CarRepairs.DAL.Models;
using CarRepairs.Model.Infrastructure;
using System.Collections.Generic;
using System.Linq;


namespace CarRepairs.Model.Repositories
{
    public interface IRepairPartRepository : IGenericRepository<RepairPart>
    {
        IEnumerable<RepairPart> GetRepairPartsByRepair(int repairID);
        IEnumerable<RepairPart> GetRepairPartsByVehicle(int vehicleID);
        IEnumerable<RepairPart> GetRepairPartsByUser(string userID);
    }

    public class RepairPartRepository : GenericRepository<RepairPart>, IRepairPartRepository
    {
        public RepairPartRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
        }

        public IEnumerable<RepairPart> GetRepairPartsByRepair(int repairID)
        {
            return dbContext.RepairParts.Where(p => p.RepairId == repairID).AsEnumerable();
        }

        public IEnumerable<RepairPart> GetRepairPartsByVehicle(int vehicleID)
        {
            return dbContext.RepairParts.Where(p => p.Repair.VehicleId == vehicleID).AsEnumerable();
        }

        public IEnumerable<RepairPart> GetRepairPartsByUser(string userID)
        {
            return dbContext.RepairParts.Where(p => p.Repair.Vehicle.UserId == userID).AsEnumerable();
        }
    }
}
