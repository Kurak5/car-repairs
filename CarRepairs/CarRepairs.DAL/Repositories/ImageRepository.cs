﻿using CarRepairs.DAL;
using CarRepairs.DAL.Models;
using CarRepairs.Model.Infrastructure;
using System.Collections.Generic;
using System.Linq;


namespace CarRepairs.Model.Repositories
{
    public interface IImageRepository : IGenericRepository<Image>
    {
        IEnumerable<Image> GetUserImages(string userID);
    }

    public class ImageRepository : GenericRepository<Image>, IImageRepository
    {
        public ImageRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {

        }

        public IEnumerable<Image> GetUserImages(string userID)
        {
            return dbContext.Images.Where(i => i.Vehicle.UserId.Equals(userID));
        }
    }
}
