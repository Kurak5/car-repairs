﻿using CarRepairs.DAL;
using CarRepairs.DAL.Models;
using CarRepairs.Model.Infrastructure;
using System.Collections.Generic;
using System.Linq;

namespace CarRepairs.Model.Repositories
{
    public interface IVehicleRepository : IGenericRepository<Vehicle>
    {
        IEnumerable<Vehicle> GetUserVehicles(string userID);
    }

    public class VehicleRepository : GenericRepository<Vehicle>, IVehicleRepository
    {
        public VehicleRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
        }

        public IEnumerable<Vehicle> GetUserVehicles(string userID)
        {
            return dbContext.Vehicles.Where(v => v.UserId == userID);
        }
    }
}
