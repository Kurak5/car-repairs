﻿using CarRepairs.DAL;
using CarRepairs.DAL.Models;
using CarRepairs.Model.Infrastructure;
using System.Collections.Generic;
using System.Linq;

namespace CarRepairs.Model.Repositories
{
    public interface IRepairRepository : IGenericRepository<Repair>
    {
        IEnumerable<Repair> GetUserRepairs(string userID);
        IEnumerable<Repair> GetRepairsByVehicle(int vehicleID);
    }

    public class RepairRepository : GenericRepository<Repair>, IRepairRepository
    {
        public RepairRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
        }

        public IEnumerable<Repair> GetRepairsByVehicle(int vehicleID)
        {
            return dbContext.Repairs.Where(r => r.Vehicle.Id == vehicleID).AsEnumerable();
        }

        public IEnumerable<Repair> GetUserRepairs(string userID)
        {
            return dbContext.Repairs.Where(r => r.Vehicle.UserId == userID).AsEnumerable();
        }
    }
}
