﻿using CarRepairs.Model.Repositories;
using System;

namespace CarRepairs.Model.Infrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        IVehicleRepository Vehicles { get; }
        IRepairRepository Repairs { get; }
        IRepairPartRepository RepairParts { get; }
        IImageRepository Images { get; }

        int Complete();
    }
}
