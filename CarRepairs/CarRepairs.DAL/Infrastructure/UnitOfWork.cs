﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRepairs.DAL;
using CarRepairs.Model.Repositories;

namespace CarRepairs.Model.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _dbContext;

        public UnitOfWork()
        {
            _dbContext = new ApplicationDbContext();
            Vehicles = new VehicleRepository(_dbContext);
            Repairs = new RepairRepository(_dbContext);
            RepairParts = new RepairPartRepository(_dbContext);
            Images = new ImageRepository(_dbContext);
        }

        public IVehicleRepository Vehicles { get; private set; }
        public IRepairRepository Repairs { get; private set; }
        public IRepairPartRepository RepairParts { get; private set; }
        public IImageRepository Images { get; private set; }

        public int Complete()
        {
            return _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
