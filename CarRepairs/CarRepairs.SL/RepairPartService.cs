﻿using CarRepairs.DAL.Models;
using System.Collections.Generic;
using CarRepairs.Model.Infrastructure;

namespace CarRepairs.SL
{
    public interface IRepairPartService
    {
        RepairPart GetRepairPart(int id, string userID);
        IEnumerable<RepairPart> GetRepairParts(int repairID);
        IEnumerable<RepairPart> GetRepairPartsByVehicle(int vehicleID);
        IEnumerable<RepairPart> GetRepairPartsByUser(string userID);
        void CreateRepairPart(RepairPart repair, string userID);
        void UpdateRepairPart(RepairPart repair, string userID);
        void RemoveRepairPart(RepairPart repair, string userID);
    }
    public class RepairPartService : IRepairPartService
    {
        private readonly IUnitOfWork _unitOfWork;

        public RepairPartService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void CreateRepairPart(RepairPart repairPart, string userID)
        {
            var repair = _unitOfWork.Repairs.GetById(repairPart.RepairId);
            if (repair.Vehicle.UserId.Equals(userID))
            {
                _unitOfWork.RepairParts.Add(repairPart);
                _unitOfWork.Complete();
            }
        }

        public RepairPart GetRepairPart(int id, string userID)
        {
            var repairPart = _unitOfWork.RepairParts.GetById(id);
            var result = repairPart?.Repair.Vehicle.UserId == userID ? repairPart : null;
            return result;
        }

        public IEnumerable<RepairPart> GetRepairParts(int repairID)
        {
            return _unitOfWork.RepairParts.GetRepairPartsByRepair(repairID);
        }

        public IEnumerable<RepairPart> GetRepairPartsByVehicle(int vehicleID)
        {
            return _unitOfWork.RepairParts.GetRepairPartsByVehicle(vehicleID);
        }

        public IEnumerable<RepairPart> GetRepairPartsByUser(string userID)
        {
            return _unitOfWork.RepairParts.GetRepairPartsByUser(userID);
        }

        public void RemoveRepairPart(RepairPart repairPart, string userID)
        {
            var repair = _unitOfWork.Repairs.GetById(repairPart.RepairId);
            if (repair.Vehicle.UserId.Equals(userID))
            {
                _unitOfWork.RepairParts.Delete(repairPart);
                _unitOfWork.Complete();
            }
        }

        public void UpdateRepairPart(RepairPart repairPart, string userID)
        {
            var repair = _unitOfWork.Repairs.GetById(repairPart.RepairId);
            if (repair.Vehicle.UserId.Equals(userID))
            {
                _unitOfWork.RepairParts.Update(repairPart);
                _unitOfWork.Complete();
            }
        }
    }
}
