﻿using CarRepairs.DAL.Models;
using System.Collections.Generic;
using CarRepairs.Model.Infrastructure;


namespace CarRepairs.SL
{
    public interface IRepairService
    {
        Repair GetRepair(int id, string userID);
        IEnumerable<Repair> GetRepairs(int vehicleID);
        IEnumerable<Repair> GetRepairsByUser(string userID);
        void CreateRepair(Repair repair, string userID);
        void UpdateRepair(Repair repair, string userID);
        void RemoveRepair(Repair repair, string userID);
    }
    public class RepairService : IRepairService
    {
        private readonly IUnitOfWork _unitOfWork;

        public RepairService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void CreateRepair(Repair repair, string userID)
        {
            var vehicle = _unitOfWork.Vehicles.GetById(repair.VehicleId);
            if (vehicle.UserId.Equals(userID))
            {
                _unitOfWork.Repairs.Add(repair);
                _unitOfWork.Complete();
            }
        }

        public Repair GetRepair(int id, string userID)
        {
            var repair = _unitOfWork.Repairs.GetById(id);
            var result = repair?.Vehicle.UserId == userID ? repair : null;
            return result;
        }

        public IEnumerable<Repair> GetRepairs(int vehicleID)
        {
            return _unitOfWork.Repairs.GetRepairsByVehicle(vehicleID);
        }

        public IEnumerable<Repair> GetRepairsByUser(string userID)
        {
            return _unitOfWork.Repairs.GetUserRepairs(userID);
        }

        public void RemoveRepair(Repair repair, string userID)
        {
            var vehicle = _unitOfWork.Vehicles.GetById(repair.VehicleId);
            if (vehicle.UserId.Equals(userID))
            {
                _unitOfWork.Repairs.Delete(repair);
                _unitOfWork.Complete();
            }
        }

        public void UpdateRepair(Repair repair, string userID)
        {
            var vehicle = _unitOfWork.Vehicles.GetById(repair.VehicleId);
            if (vehicle.UserId.Equals(userID))
            {
                _unitOfWork.Repairs.Update(repair);
                _unitOfWork.Complete();
            }
        }
    }
}
