﻿using CarRepairs.DAL.Models;
using System.Collections.Generic;
using CarRepairs.Model.Infrastructure;
using System.IO;
using System;
using System.Data;

namespace CarRepairs.SL
{
    public interface IImageService
    {
        Image GetImage(int id, string userID);
        IEnumerable<Image> GetImages(string userID);
        string Create(Image image, string fileName, string userID);
        void Update(Image image, string userID);
        void Remove(Image image, string userID);
    }

    class ImageService : IImageService
    {
        private readonly IUnitOfWork _unitOfWork;
        public string LocalPath { get; set; } = "~/Content/Images/";
        public ImageService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public string Create(Image image, string fileName, string UserID)
        {
            string name = Path.GetFileNameWithoutExtension(fileName);
            string extention = Path.GetExtension(fileName);
            name += DateTime.Now.ToString("yyyMdHmsfff") + extention;
            image.Path = LocalPath + name;

            var dataImage = _unitOfWork.Images.GetById(image.Id);
            if (dataImage == null)
            {
                var vehicle = _unitOfWork.Vehicles.GetById(image.Id);
                if(vehicle != null && vehicle.UserId.Equals(UserID))
                {
                    _unitOfWork.Images.Add(image);
                    _unitOfWork.Complete();
                    return image.Path;
                }
            }
            else
            {
                dataImage.Title = image.Title;
                dataImage.Path = image.Path;
                _unitOfWork.Complete();
                return image.Path;
            }
            throw new DataException();
        }

        public Image GetImage(int id, string userID)
        {
            var dataImage = _unitOfWork.Images.GetById(id);
            var result = dataImage?.Vehicle.UserId ==userID ? dataImage : null;
            return result;
        }

        public IEnumerable<Image> GetImages(string userID)
        {
            return _unitOfWork.Images.GetUserImages(userID);
        }

        public void Remove(Image image, string userID)
        {
            var dataImage = _unitOfWork.Images.GetById(image.Id);
            if (dataImage.Vehicle.UserId.Equals(userID))
            {
                _unitOfWork.Images.Delete(image);
                _unitOfWork.Complete();
            }
        }

        public void Update(Image image, string userID)
        {
            var dataImage = _unitOfWork.Images.GetById(image.Id);
            if (dataImage.Vehicle.UserId.Equals(userID))
            {
                _unitOfWork.Images.Update(image);
                _unitOfWork.Complete();
            }
        }
    }
}
