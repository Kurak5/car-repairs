﻿using CarRepairs.DAL.Models;
using System.Collections.Generic;
using CarRepairs.Model.Infrastructure;


namespace CarRepairs.SL
{
    public interface IVehicleService
    {
        Vehicle GetVehicle(int id, string userID);
        IEnumerable<Vehicle> GetVehicles(string userID);
        void Create(Vehicle vehicle, string userID);
        void Update(Vehicle vehicle, string userID);
        void Remove(Vehicle vehicle, string userID);
    }

    public class VehicleService : IVehicleService
    {
        private readonly IUnitOfWork _unitOfWork;

        public VehicleService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Create(Vehicle vehicle, string UserID)
        {
            vehicle.UserId = UserID;
            _unitOfWork.Vehicles.Add(vehicle);
            _unitOfWork.Complete();
        }

        public Vehicle GetVehicle(int id, string userID)
        {
            var vehicle = _unitOfWork.Vehicles.GetById(id);
            var result = vehicle?.UserId == userID ? vehicle : null;
            return result;
        }

        public IEnumerable<Vehicle> GetVehicles(string userID)
        {
            var result = _unitOfWork.Vehicles.GetUserVehicles(userID);
            return result;
        }

        public void Remove(Vehicle vehicle, string UserID)
        {
            if(vehicle.UserId == UserID)
            {
                _unitOfWork.Vehicles.Delete(vehicle);
                _unitOfWork.Complete();
            }
        }

        public void Update(Vehicle vehicle, string UserID)
        {
            if (vehicle.UserId == UserID)
            {
                _unitOfWork.Vehicles.Update(vehicle);
                _unitOfWork.Complete();
            }
        }
    }
}
