﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CarRepairs.UI.Startup))]
namespace CarRepairs.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
