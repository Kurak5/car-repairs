﻿using CarRepairs.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarRepairs.UI.ViewModels
{
    public class UserRepairsViewModel
    {
        public List<RepairModel> Repairs { get; set; }
    }

}