﻿using CarRepairs.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarRepairs.UI.ViewModels
{
    public class VehicleRepairViewModel
    {
        public string VehicleName { get; set; }
        public RepairModel Repair { get; set; }
        public List<RepairPartModel> RepairParts { get; set; }
    }
}