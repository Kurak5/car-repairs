﻿using CarRepairs.UI.Models;

namespace CarRepairs.UI.ViewModels
{
    public class VehicleImageViewModel
    {
        public VehicleModel Vehicle { get; set; }
        public ImageModel Image { get; set; }
    }
}