﻿using CarRepairs.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarRepairs.UI.ViewModels
{
    public class UserVehiclesImagesViewModel
    {
        public List<VehicleImageViewModel> VehicleImageModels { get; set; }
        public UserVehiclesImagesViewModel()
        {
            VehicleImageModels = new List<VehicleImageViewModel>();
        }
    }

    public class UserVehiclesViewModel
    {
        public List<VehicleModel> Vehicles { get; set; }
        public UserVehiclesViewModel()
        {
            Vehicles = new List<VehicleModel>();
        }
    }
}