﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace CarRepairs.UI.Models
{
    public class ImageModel
    {
        public int Id { get; set; }

        [Display(Name = "Tytuł obrazka")]
        public string Title { get; set; }

        [Display(Name = "Dodaj plik")]
        public string Path { get; set; }

        public HttpPostedFileBase ImageFile { get; set; }

        public ImageModel()
        {
            Path = "~/Content/Images/default.png";
        }
    }
}