﻿using CarRepairs.DAL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarRepairs.UI.Models
{
    public class RepairPartModel
    {
        public int Id { get; set; }

        [Display(Name = "Nazwa części")]
        [Required(ErrorMessage = "Podaj nazwę części")]
        public string Name { get; set; }

        [Display(Name = "Koszt")]
        [DisplayFormat(DataFormatString = "{0:C0}")]
        [Required(ErrorMessage = "Podaj całkowity koszt. (PLN)")]
        [Range(0, int.MaxValue, ErrorMessage = "Cena nie może być ujemna!")]
        public decimal Cost { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Dodatkowe informacje")]
        public string Adnotation { get; set; }

        public int RepairId { get; set; }

        public virtual RepairModel Repair { get; set; }
    }
}