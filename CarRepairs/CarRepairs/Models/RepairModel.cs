﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarRepairs.UI.Models
{
    public class RepairModel
    {
        public int Id { get; set; }

        [Display(Name = "Nazwa naprawy")]
        [Required(ErrorMessage = "Podaj krótką nazwę naprawy.")]
        public string Name { get; set; }

        [Display(Name = "Miejsce naprawy")]
        public string Place { get; set; }

        [Display(Name = "Przebieg przy naprawie (km)")]
        [Range(0, int.MaxValue, ErrorMessage = "Przebieg nie może być ujemny!")]
        public int? VehicleMileage { get; set; }

        [Display(Name = "Robocizna")]
        [DisplayFormat(DataFormatString = "{0:C0}")]
        [Required(ErrorMessage = "Podaj koszt robocizny. (PLN)")]
        [Range(0, int.MaxValue, ErrorMessage = "Cena nie może być ujemna!")]
        public int Labor { get; set; }

        [Display(Name = "Data naprawy")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Podaj datę naprawy.")]
        public DateTime Date { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Dodatkowe informacje")]
        public string Adnotation { get; set; }

        public int VehicleId { get; set; }

        public virtual VehicleModel Vehicle { get; set; }

        public virtual List<RepairPartModel> RepairParts { get; set; }

        public RepairModel()
        {
            RepairParts = new List<RepairPartModel>();
        }

        public decimal GetRepairPartsCost()
        {
            decimal cost = 0;
            foreach (var item in RepairParts)
                cost += item.Cost;

            return cost;
        }

        public string GetMileage()
        {
            return VehicleMileage.Equals(null) ? "-" : VehicleMileage.ToString() + " km";
        }
    }
}