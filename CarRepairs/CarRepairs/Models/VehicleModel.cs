﻿using CarRepairs.DAL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarRepairs.UI.Models
{
    public class VehicleModel
    {
        public int Id { get; set; }

        [Display(Name = "Marka")]
        [Required(ErrorMessage = "Podaj markę pojazdu.")]
        public string Producent { get; set; }

        [Display(Name = "Model")]
        [Required(ErrorMessage = "Podaj model pojazdu.")]
        public string ModelName { get; set; }

        [Display(Name = "Rok produkcji (r.)")]
        [Range(1800, int.MaxValue, ErrorMessage = "Podaj prawidłowy rocznik.")]
        public int? ProductionYear { get; set; }

        [Display(Name = "Przebieg (km)")]
        [Required(ErrorMessage = "Podaj aktualny przebieg pojazdu.")]
        [Range(0, int.MaxValue, ErrorMessage = "Przebieg nie może być ujemny!")]
        public int Mileage { get; set; }

        [Display(Name = "Pojemność silnika (cm\xB3)")]
        [Range(0, int.MaxValue, ErrorMessage = "Pojemność nie może być ujemna!")]
        public int? EngineCapacity { get; set; }

        [Display(Name = "Moc silnika (KM)")]
        [Range(0, int.MaxValue, ErrorMessage = "Moc nie może być ujemna!")]
        public int? EnginePower { get; set; }

        [Display(Name = "Data zakupu")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Required(ErrorMessage = "Podaj datę zakupu.")]
        public DateTime PurchaseDate { get; set; }

        [Display(Name = "Cena zakupu")]
        [DisplayFormat(DataFormatString = "{0:C0}")]
        [Required(ErrorMessage = "Podaj cenę zakupu. (PLN)")]
        [Range(0, int.MaxValue, ErrorMessage = "Cena nie może być ujemna!")]
        public decimal PurchasePrice { get; set; }

        public string UserId { get; set; }

        public virtual List<RepairModel> Repairs { get; set; }

        public VehicleModel()
        {
            Repairs = new List<RepairModel>();
        }
    }
}