﻿using AutoMapper;
using CarRepairs.UI.Models;
using CarRepairs.SL;
using CarRepairs.DAL.Models;
using CarRepairs.UI.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CarRepairs.UI.Controllers
{
    [Authorize]
    public class RepairController : Controller
    {
        readonly IRepairService _repairService;
        readonly IVehicleService _vehicleService;
        readonly IRepairPartService _repairPartService;
        public RepairController(IRepairService repairService, IVehicleService vehicleService, IRepairPartService repairPartService)
        {
            _repairService = repairService;
            _vehicleService = vehicleService;
            _repairPartService = repairPartService;
        }

        public ViewResult DisplayList()
        {
            var dataRepair = _repairService.GetRepairsByUser(User.Identity.GetUserId()).ToList();
            List<RepairModel> repairModels = new List<RepairModel>();
            Mapper.Map(dataRepair, repairModels);

            var viewModel = new UserRepairsViewModel
            {
                Repairs = repairModels
            };
            return View(viewModel);
        }

        public ActionResult Create(int? vehicleId)
        {
            if (vehicleId == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var dataVehicle = _vehicleService.GetVehicle((int)vehicleId, User.Identity.GetUserId());
            if(dataVehicle == null)
                return HttpNotFound();

            RepairModel repairModel = new RepairModel
            {
                VehicleId = dataVehicle.Id,
                Date = DateTime.Today
            };

            VehicleRepairViewModel vehicleRepairViewModel = new VehicleRepairViewModel
            {
                VehicleName = dataVehicle.Producent + " " + dataVehicle.ModelName,
                Repair = repairModel,
            };

            return View(vehicleRepairViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VehicleRepairViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Repair repair = new Repair();
                    Mapper.Map(model.Repair, repair);
                    _repairService.CreateRepair(repair, User.Identity.GetUserId());
                    return RedirectToAction("Create", "RepairPart", new {repairID = repair.Id });
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Nie udało się zapisać zmian. Spróbuj ponownie później, jeżeli problem nie ustępuje skontaktuj się z administratorem.");
            }

            return View(model);
        }

        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (saveChangesError.GetValueOrDefault())
                ViewBag.ErrorMessage = "Nie udało się usunąć danych. Spróbuj ponownie później, jeżeli problem nie ustępuje skontaktuj się z administratorem.";

            var dataRepair = _repairService.GetRepair((int)id, User.Identity.GetUserId());
            if (dataRepair == null)
                return HttpNotFound();
            
            RepairModel repairModel = new RepairModel();
            Mapper.Map(dataRepair, repairModel);
            VehicleRepairViewModel vehicleRepairViewModel = new VehicleRepairViewModel
            {
                VehicleName = dataRepair.Vehicle.Producent + " " + dataRepair.Vehicle.ModelName,
                Repair = repairModel,
            };

            return View(vehicleRepairViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                var dataRepair = _repairService.GetRepair(id, User.Identity.GetUserId());
                _repairService.RemoveRepair(dataRepair, User.Identity.GetUserId());
            }
            catch (DataException)
            {
                return RedirectToAction("Delete", new { id, saveChangesError = true });
            }
            return RedirectToAction("DisplayList");
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var dataRepair = _repairService.GetRepair((int)id, User.Identity.GetUserId());
            if (dataRepair == null)
                return HttpNotFound();

            RepairModel repairModel = new RepairModel();
            Mapper.Map(dataRepair, repairModel);

            var repairParts = _repairPartService.GetRepairParts(dataRepair.Id).ToList();
            List<RepairPartModel> repairPartModels = new List<RepairPartModel>();
            Mapper.Map(repairParts, repairPartModels);

            VehicleRepairViewModel vehicleRepairViewModel = new VehicleRepairViewModel
            {
                VehicleName = dataRepair.Vehicle.Producent + " " + dataRepair.Vehicle.ModelName,
                Repair = repairModel,
                RepairParts = repairPartModels,
            };

            return View(vehicleRepairViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VehicleRepairViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Repair repair = new Repair();
                    Mapper.Map(model.Repair, repair);
                    _repairService.UpdateRepair(repair, User.Identity.GetUserId());
                    return RedirectToAction("DisplayList");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Nie udało się zapisać zmian. Spróbuj ponownie, jeżeli problem nie ustępuje skontaktuj się z administratorem.");
            }
            return View(model);
        }
    }
}