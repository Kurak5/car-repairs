﻿using AutoMapper;
using CarRepairs.DAL.Models;
using CarRepairs.UI.Models;
using CarRepairs.SL;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CarRepairs.UI.Controllers
{
    [Authorize]
    public class RepairPartController : Controller
    {
        private IRepairPartService _repairPartService;
        private IRepairService _repairService;


        public RepairPartController(IRepairPartService repairPartService, IRepairService repairService)
        {
            _repairPartService = repairPartService;
            _repairService = repairService;
        }

        public ActionResult Create(int? repairID)
        {
            if (repairID == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var dataRepair = _repairService.GetRepair((int)repairID, User.Identity.GetUserId());
            if (dataRepair == null)
                return HttpNotFound();

            RepairPartModel repairPartModel = new RepairPartModel
            {
                RepairId = dataRepair.Id,
            };

            return View(repairPartModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RepairPartModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    RepairPart repairPart = new RepairPart();
                    Mapper.Map(model, repairPart);
                    _repairPartService.CreateRepairPart(repairPart, User.Identity.GetUserId());
                    return RedirectToAction("Create", "RepairPart", new { repairID = repairPart.RepairId }); 
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Nie udało się zapisać zmian. Spróbuj ponownie później, jeżeli problem nie ustępuje skontaktuj się z administratorem.");
            }

            return View(model);
        }

        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (saveChangesError.GetValueOrDefault())
                ViewBag.ErrorMessage = "Nie udało się usunąć danych. Spróbuj ponownie później, jeżeli problem nie ustępuje skontaktuj się z administratorem.";

            var dataRepairPart = _repairPartService.GetRepairPart((int)id, User.Identity.GetUserId());
            if (dataRepairPart == null)
                return HttpNotFound();

            RepairPartModel repairPartModel = new RepairPartModel();
            Mapper.Map(dataRepairPart, repairPartModel);

            return View(repairPartModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                var dataRepairPart = _repairPartService.GetRepairPart(id, User.Identity.GetUserId());
                _repairPartService.RemoveRepairPart(dataRepairPart, User.Identity.GetUserId());
            }
            catch (DataException)
            {
                return RedirectToAction("Delete", new { id, saveChangesError = true });
            }
            return RedirectToAction("DisplayList"); //TODO
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var dataRepairPart = _repairPartService.GetRepairPart((int)id, User.Identity.GetUserId());
            if (dataRepairPart == null)
                return HttpNotFound();

            RepairPartModel repairPartModel = new RepairPartModel();
            Mapper.Map(dataRepairPart, repairPartModel);

            return View(repairPartModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RepairModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    RepairPart repairPart = new RepairPart();
                    Mapper.Map(model, repairPart);
                    _repairPartService.UpdateRepairPart(repairPart, User.Identity.GetUserId());
                    return RedirectToAction("DisplayList");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Nie udało się zapisać zmian. Spróbuj ponownie, jeżeli problem nie ustępuje skontaktuj się z administratorem.");
            }
            return View(model);
        }
    }
}