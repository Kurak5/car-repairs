﻿using CarRepairs.UI.Models;
using CarRepairs.SL;
using CarRepairs.DAL.Models;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using AutoMapper;
using System.Data;

namespace CarRepairs.UI.Controllers
{
    [Authorize]
    public class ImageController : Controller
    {
        readonly IImageService _imageService;
        readonly IVehicleService _vehicleService;
        public ImageController(IImageService imageService, IVehicleService vehicleService)
        {
            _imageService = imageService;
            _vehicleService = vehicleService;
        }

        public ActionResult Create(int? vehicleID)
        {
            if (vehicleID == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var dataVehicle = _vehicleService.GetVehicle((int)vehicleID, User.Identity.GetUserId());
            if (dataVehicle == null)
                return HttpNotFound();

            ImageModel imageModel = new ImageModel
            {
                Id = dataVehicle.Id,
            };

            return View(imageModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ImageModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string oldFileLoaclPath = _imageService.GetImage(model.Id, User.Identity.GetUserId())?.Path;

                    Image image = new Image();
                    Mapper.Map(model, image);
                    string fileName = _imageService.Create(image, model.ImageFile.FileName, User.Identity.GetUserId());

                    WebImage img = new WebImage(model.ImageFile.InputStream);
                    if (img.Width > 320)
                        img.Resize(320, 240);
                    var filePath = Server.MapPath(fileName);
                    img.Save(filePath);
                    
                    var oldFilePath = Server.MapPath(oldFileLoaclPath);
                    if (System.IO.File.Exists(oldFilePath))
                        System.IO.File.Delete(oldFilePath);

                    return RedirectToAction("Edit", "Vehicle", new { id = model.Id });
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Nie udało się zapisać zmian. Spróbuj ponownie później, jeżeli problem nie ustępuje skontaktuj się z administratorem.");
            }

            return View(model);
        }

    }
}