﻿using AutoMapper;
using CarRepairs.DAL.Models;
using CarRepairs.UI.Models;
using CarRepairs.SL;
using CarRepairs.UI.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CarRepairs.UI.Controllers
{
    [Authorize]
    public class VehicleController : Controller
    {
        readonly IVehicleService _vehicleService;
        readonly IImageService _imageService;
        public VehicleController(IVehicleService vehicleService, IImageService imageService)
        {
            _vehicleService = vehicleService;
            _imageService = imageService;
        }

        public ViewResult DisplayList()
        {
            var dataVehicles = _vehicleService.GetVehicles(User.Identity.GetUserId()).ToList();

            List<VehicleModel> vehicles = new List<VehicleModel>();
            Mapper.Map(dataVehicles, vehicles);

            var viewModel = new UserVehiclesViewModel
            {
                Vehicles = vehicles,
            };
            return View(viewModel);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VehicleModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Vehicle dataModel = new Vehicle();
                    Mapper.Map(model, dataModel);
                    _vehicleService.Create(dataModel, User.Identity.GetUserId());
                    return RedirectToAction("DisplayList");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Nie udało się zapisać zmian. Spróbuj ponownie, jeżeli problem nie ustępuje skontaktuj się z administratorem.");
            }

            return View();
        }

        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (saveChangesError.GetValueOrDefault())
                ViewBag.ErrorMessage = "Nie udało się odczytać danych. Spróbuj ponownie, jeżeli problem nie ustępuje skontaktuj się z administratorem.";

            var dataVehicle = _vehicleService.GetVehicle((int)id, User.Identity.GetUserId());
            if (dataVehicle == null)
                return HttpNotFound();

            VehicleModel vehicle = new VehicleModel();
            Mapper.Map(dataVehicle, vehicle);
            return View(vehicle);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                var dataVehicle = _vehicleService.GetVehicle(id, User.Identity.GetUserId());
                _vehicleService.Remove(dataVehicle, User.Identity.GetUserId());
            }
            catch (DataException)
            {
                return RedirectToAction("Delete", new { id, saveChangesError = true });
            }
            return RedirectToAction("DisplayList");
        }

        public ActionResult Edit(int? id, bool? saveChangesError = false)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (saveChangesError.GetValueOrDefault())
                ViewBag.ErrorMessage = "Nie udało się odczytać danych. Spróbuj ponownie, jeżeli problem nie ustępuje skontaktuj się z administratorem.";

            var dataVehicle = _vehicleService.GetVehicle((int)id, User.Identity.GetUserId());
            if (dataVehicle == null)
                return HttpNotFound();

            VehicleModel vehicle = new VehicleModel();
            Mapper.Map(dataVehicle, vehicle);

            var dataImage = _imageService.GetImage(vehicle.Id, User.Identity.GetUserId());
            ImageModel image = new ImageModel();
            if(dataImage != null)
                Mapper.Map(dataImage, image);

            VehicleImageViewModel vehicleImageView = new VehicleImageViewModel
            {
                Vehicle = vehicle,
                Image = image,
            };
            return View(vehicleImageView);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VehicleImageViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Vehicle dataVehicle = new Vehicle();
                    Mapper.Map(model.Vehicle, dataVehicle);
                    _vehicleService.Update(dataVehicle, User.Identity.GetUserId());
                    return RedirectToAction("DisplayList");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Nie udało się zapisać zmian. Spróbuj ponownie, jeżeli problem nie ustępuje skontaktuj się z administratorem.");
            }
            return View(model);
        }
    }
}