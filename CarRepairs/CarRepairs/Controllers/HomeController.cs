﻿using AutoMapper;
using CarRepairs.DAL.Models;
using CarRepairs.UI.Models;
using CarRepairs.SL;
using CarRepairs.UI.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarRepairs.UI.Controllers
{
    public class HomeController : Controller
    {
        readonly IVehicleService _vehicleService;
        readonly IImageService _imageService;
        public HomeController(IVehicleService vehicleService, IImageService imageService)
        {
            _vehicleService = vehicleService;
            _imageService = imageService;
        }

        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                var dataVehicles = _vehicleService.GetVehicles(User.Identity.GetUserId()).ToList();
                List<VehicleModel> vehicles = new List<VehicleModel>();
                Mapper.Map(dataVehicles, vehicles);

                var dataImages = _imageService.GetImages(User.Identity.GetUserId()).ToList();
                List<ImageModel> images = new List<ImageModel>();
                Mapper.Map(dataImages, images);

                var viewModel = new UserVehiclesImagesViewModel();
                foreach (var v in vehicles)
                {
                    viewModel.VehicleImageModels.Add(new VehicleImageViewModel
                    {
                        Vehicle = v,
                        Image = images.FirstOrDefault(i => i.Id == v.Id) ?? new ImageModel(),
                    });
                }
                    
                return View(viewModel);
            }
            else
                return View();
        }

    }
}