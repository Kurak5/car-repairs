﻿using AutoMapper;
using CarRepairs.DAL.Models;
using CarRepairs.UI.Models;

namespace CarRepairs.UI
{
    public class AutoMapperConfig : Profile
    {
        public static void Run()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<AutoMapperConfig>();
                cfg.AllowNullCollections = true;
            });
        }

        public AutoMapperConfig()
        {
            //Domain to view profile
            CreateMap<Vehicle, VehicleModel>();
            CreateMap<Repair, RepairModel>();
            CreateMap<RepairPart, RepairPartModel>();
            CreateMap<Image, ImageModel>();


            //View to domain profile
            CreateMap<VehicleModel, Vehicle>();
            CreateMap<RepairModel, Repair>();
            CreateMap<RepairPartModel, RepairPart>();
            CreateMap<ImageModel, Image>();
        }
    }
}